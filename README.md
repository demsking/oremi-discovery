# Oremi Discovery

[![Buy me a beer](https://img.shields.io/badge/Buy%20me-a%20beer-1f425f.svg)](https://www.buymeacoffee.com/demsking)

Oremi Discovery is a library for facilitating device and service discovery and
communication in IoT networks using the MQTT (Message Queuing Telemetry
Transport) protocol. It provides a lightweight and efficient solution for
discovering and managing devices and services on a network, enabling seamless
communication between them.

Built on top of the Paho MQTT library and utilizing the Oremi Core package,
Oremi Discovery simplifies the process of discovering and integrating IoT
devices and services into larger systems. This library is designed with
scalability, reliability, and ease of use in mind, making it suitable for both
small-scale and enterprise-level IoT deployments.

## Getting Started with Oremi Discovery

- [Oremi Discovery for Python](https://gitlab.com/demsking/oremi-discovery/-/tree/main/python)

<!-- - [Oremi Discovery for Dart](https://gitlab.com/demsking/oremi-discovery/-/tree/main/dart) -->

## Contribute

Please follow [CONTRIBUTING.md](https://gitlab.com/demsking/oremi-discovery/blob/main/CONTRIBUTING.md).

## Versioning

Given a version number `MAJOR.MINOR.PATCH`, increment the:

- `MAJOR` version when you make incompatible API changes,
- `MINOR` version when you add functionality in a backwards-compatible manner,
  and
- `PATCH` version when you make backwards-compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions
to the `MAJOR.MINOR.PATCH` format.

See [SemVer.org](https://semver.org/) for more details.

## License

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License.
You may obtain a copy of the License at [LICENSE](https://gitlab.com/demsking/oremi-discovery/blob/main/LICENSE).
